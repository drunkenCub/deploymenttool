# Created on 09-07-2014
# By Chathuranga Bandara
# Make sure MSDEPLOY, MSBUILD and nuget are installed in the system <-- This is Mandatory 
# Make sure Python 2.7 is installed <-- Mandatory 
# USAGE: 
#		1. Change configurations in config.py
# 		2. Run this python script
#		3. this will use msbuilder.py 
#		4. Make Sure the Package.zip is in the same folder
#		5. UserName: KTV_Deployment Pass: ktv123
#
#	example usage command = python deploymentToo.py KTV_Deployment ktv123 D:\Projects\bluetag\Application\trunk\Ktv_Oms.sln 

import os, shlex, subprocess, re, datetime, config, msbuilder, sys

def startdeployment():
	__userName = config.USERNAME
	__password = config.PASSWORD
	#msdeploy = config.MSDEPLOY
	
	if sys.argv[1] == __userName and sys.argv[2] == __password:
		deployagent = msbuilder.MsBuilder()
		deployagent.run(sys.argv[3], sys.argv[3])
	
startdeployment()


 
