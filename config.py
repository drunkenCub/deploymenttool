# Configurations for the deploymenttool.py
# Added by Chathuranga on 09-07-2014

# binary configs
MSDEPLOY = r'C:\MSDeploy\msdeploy.exe' #configure msdeploy path
MSBUILD = r'C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe' #configure msbuild path
MSTEST = r'C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\MSTest.exe' #configure mstest path
NUGET = r'C:\NuGet\NuGet.exe' #configure nuget path

#User Configs
PASSWORD = 'ktv123'
USERNAME = 'KTV_Deployment'

#Package Location path
PACKAGELOC = r"Package.zip"



